#!/usr/bin/env ruby
# frozen-string-literal: true

#require 'Sugilite/version'
# require 'Sugilite/Sugilite'
require 'oj'
require 'net/http'
require 'uri'
require 'pry'
local_dir = File.expand_path('../')
$LOAD_PATH.unshift(local_dir)
require 'sugiliteIRC'
require 'sugiliteKraken'

# Work with Twitch TMI to pull data
module Sugilite
  # Errors?
  class Error < StandardError; end
  # Get the user list from TMI all methods return arrays
  class GetUserList
    # Channel name.
    attr_accessor :channel

    # New message
    def initialize(channel)
      # Users Channel
      @url = URI("https://tmi.twitch.tv/group/user/#{channel}/chatters")
    end

    # list all users on channel
    def all
      userlist = []
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters')
      userary.keys.each { |k| userary[k].each { |user| userlist.push(user) } }
      userlist
    end
    
    # Pick a random viewer of your channel, with out any special ranks.
    def randomviewer
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('viewers')
      userary.sample
    end

    # Pick a random viewer from all in channel.
    def random
      userlist = []
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters')
      userary.keys.each { |k| userary[k].each { |user| userlist.push(user) } }
      userlist.sample
    end

    # lists the broadcaster in channel
    def broadcaster
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('broadcaster')
      userary
    end

    # lists any vips in channel
    def vip
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('vips')
      userary
    end

    # lists any moderators in channel
    def moderator
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('moderators')
      userary
    end

    # lists any staff in channel
    def staff
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('staff')
      userary
    end

    # lists any admins in channel
    def admin
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('admins')
      userary
    end

    # lists any global moderators in channel (possibly depricated)
    def global_mod
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('global_mods')
      userary
    end

    # lists viewers
    def viewer
      getchan = Net::HTTP.get(@url)
      userary = Oj.load(getchan).fetch('chatters').fetch('viewers')
      userary
    end
  end
end
