#!/usr/bin/env ruby
# frozen_string_literal: true

require 'oj'
require 'net/http'
require 'uri'

# Twitch Kraken API tools
module Sugilite
  # retrieve channel chatrooms from Twitch Kraken
  class Kraken
    # direct access to the response from net::http
    attr_accessor(:response)

    # get things up and running
    def initialize(*channel_id, client_id, auth)
      # channel id of Twitch Channel
      @channel_id = channel_id
      # client id of user trying to access Twitch api
      @client_id = client_id
      # OAuth of person trying to access Twitch API
      @auth = "OAuth #{auth}"
    end

    # Convert username to userid
    def getuserid(username)
      uri = URI.parse("https://api.twitch.tv/kraken/users?login=#{username}")
      request = Net::HTTP::Get.new(uri)
      request['Accept'] = 'application/vnd.twitchtv.v5+json'
      request['Client-Id'] = @client_id

      req_options = {
          use_ssl: uri.scheme == 'https'
      }
      @response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end
      id = Oj.load(@response.body, mode: :object)
      id.fetch('users')[0].fetch('_id')
    end

    # Get chat rooms for user id specified
    def getrooms
      uri = URI.parse("https://api.twitch.tv/kraken/chat/#{@channel_id[0]}/rooms")
      request = Net::HTTP::Get.new(uri)
      request['Accept'] = 'application/vnd.twitchtv.v5+json'
      request['Client-Id'] = @client_id
      request['Authorization'] = @auth

      req_options = {
          use_ssl: uri.scheme == 'https'
      }
      @response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end
      Oj.load(@response.body, mode: :object)
    end
  end
end
