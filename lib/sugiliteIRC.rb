#!/usr/bin/env ruby
# frozen_string_literal: true

require 'oj'

# rubocop:disable Metrics/MethodLength, Metrics/AbcSize

# Obtains information about IRC messages
module SugiliteIRC
  # Displays contents of the message prefix
  class Prefix
    # User's real name.
    attr_reader :realname
    # User's username.
    attr_reader :username
    # User's hostname
    attr_reader :hostname
    # Initialize a new message prefix
    def initialize(data)
      @realname = @username = @hostname = ''
      if data[0] == ':'
        data2 = data[1..-1].split(/[!@]/)
        @realname = data2[0]
        @username = data2[1]
        @hostname = data2[2]
      end
    end
  end

  # Parses standard IRC messages
  class Message
    # Raw message
    attr_reader :raw
    # Message tags
    attr_reader :tags
    # Message parameters
    attr_reader :parameters
    # Message prefix
    attr_reader :prefix
    # Message commands (i.e. PING and PRIVMSG)
    attr_reader :command
    # Initalizes message
    def initialize(data)
      @raw = data
      @tags = {}
      @parameters = []
      @prefix = nil
      data = data.split(' ')
      data2 = data.shift
      if data2[0] == '@'
        data2[1..-1].split(';').each { |r_tag| s_tag = r_tag.split('=', 2); @tags[s_tag[0]] = s_tag[1] }
        data2 = data.shift
      end
      if data2[0] == ':'
        @prefix = Prefix.new(data2)
        data2 = data.shift
      end
      @command = data2
      loop do
        if data[0].nil?
          break
        elsif data[0][0] == ':'
          @parameters.push(data.join(' ')[1..-1])
          break
        elsif data.length == 1
          @parameters.push(data[0])
          break
        else
          @parameters.push(data.shift)
        end
      end
    end

    # Dumps from JSON object to String.
    def to_s
      Oj.dump self, indent: 2
    end

    # Gets Channel from irc data.
    def channel
      case @command
        when 'PRIVMSG', 'NOTICE', 'HOSTTARGET', 'ROOMSTATE', 'USERNOTICE', 'USERSTATE'
          @parameters[0]
        else
          nil
      end
    end

    # Gets the sender.
    def sender
      case @command
        when 'PRIVMSG', 'NOTICE'
          @prefix.realname
        else
          nil
      end
    end

    # Gets message contents as a string.
    def message
      case @command
        when 'PRIVMSG', 'NOTICE'
          @parameters[1]
        else
          nil
      end
    end
  end

  # Extra parsing for Twitch.tv's uniqueness.
  class TwitchMessage < Message
    # Get total bits in message if there are bits otherwise return empty value
    def bits
      @tags['bits'].to_i
    end

    # Is the user a moderator or broadcaster?
    def moderator?
      (@tags['badges'].include? 'moderator') || broadcaster?
    end

    # Is the user a broadcaster?
    def broadcaster?
      @tags['badges'].include? 'broadcaster'
    end

    # Is the user a subscriber?
    def subscriber?
      @tags['badges'].include? 'subscriber'
    end

    # Is the user a Twitch Turbo user?
    def turbo?
      @tags['badges'].include? 'turbo'
    end

    # Is the user a Twitch Prime user?
    def prime?
      @tags['badges'].include? 'premium'
    end

    # Is the user a partner?
    def partner?
      @tags['badges'].include? 'partner'
    end

    # Is the user a vip?
    def vip?
      @tags['badges'].include? 'vip'
    end

    # Is the user an admin?
    def admin?
      @tags['badges'].include? 'admin'
    end

    # Is the user a global moderator?
    def globalmod?
      @tags['badges'].include? 'global_mod'
    end

    # Is the user a staff?
    def staff?
      @tags['badges'].include? 'staff'
    end

    # Get color of username in hex value if unset by user will return empty string.
    def color
      @tags['color']
    end
  end
end
# rubocop:enable Metrics/MethodLength, Metrics/AbcSize
