lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "sugilite/version"

Gem::Specification.new do |spec|
  spec.name          = "sugilite"
  spec.version       = Sugilite::VERSION
  spec.authors       = ["MediaMagnet"]
  spec.email         = ["nathanec@gmail.com"]

  spec.summary       = 'Twitch chat client gem using websockets'
  spec.description   = 'Twitch chat client gem using websockets and twitch irc'
  spec.homepage      = 'https://gitlab.com/MediaMagnet/Sugilite'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 2.6.3'

  spec.metadata["allowed_push_host"] = 'https://rubygems.org'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/MediaMagnet/Sugilite'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/MediaMagnet/Sugilite/blob/Sugilite/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.extensions    = ["ext/sugilite/extconf.rb"]

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'oj', '~> 3.9.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rake-compiler', '~> 1.0.7'
  spec.add_development_dependency 'rest-client', '~> 2.1.0'
end
